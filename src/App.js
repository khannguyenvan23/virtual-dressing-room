import React from 'react';
import './App.css';
import Header from './app/Header';
import ShopDisplay from './app/ShopDisplay';
import Model from './app/Model';
import Footer from './app/Footer';

function App() {
  return (

    <>
  <div className="container-fluid">
    <Header />
    <div className="row">
      <div className="col-lg-6 col-sm-6">
        <ShopDisplay />
      </div>
      <div className="col-lg-6 col-sm-6">
        <Model />
      </div>
    </div>
  </div>
    <Footer />
</>

  );
}

export default App;
