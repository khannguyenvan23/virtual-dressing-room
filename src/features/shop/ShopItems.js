import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { selectShopByType, handleChooseItem } from './ShopSlice';

const RenderItem = ({ id , name, img }) =>{
  const dispatch = useDispatch()
  const handleIdDispatch = (id) =>{
    dispatch(handleChooseItem({id}))
  }
  return (
    <div className="card" style={{width: "18rem"}}>
      <img src={img}
        alt={name}
      />
       <div className="card-body">
        <h5 className="card-title">{name}</h5>
        <button onClick={()=>handleIdDispatch(id)} className="btn btn-primary">Thử Hàng</button>
       </div>
      <p></p>
    </div>
  )
};

export function ShopItems({ type }) {
  const getShopItem = useSelector((state)=>selectShopByType(state, type))
  const mapRenderItem = getShopItem.map((item) => (
    <RenderItem
      id={item.id}
      name={item.name}
      img={item.imgSrc_jpg}
      key={item.id}/>
  ))
  return (
   <>
    {mapRenderItem}
   </>
  );
}
