import { createSlice } from '@reduxjs/toolkit';
import Data from '../../Data/Data'
export const shopSlice = createSlice({
  name: 'shop',
  initialState: {
    data: Data,
    defaultChoose: {
      body: {
        background:'url("/images/allbody/bodynew.png")'
       },
      bikinitop: {
        background:'url("/images/allbody/bikini_branew.png")'
       },
      bikinibottom: {
        background:'url("/images/allbody/bikini_pantsnew.png")'
       },
      model: {
        background:'url("/images/model/1000new.png")'
       },
      feetleft: {
        background:'url("/images/allbody/feet_high_leftnew.png")'
       },
      feetright: {
        background:'url("/images/allbody/feet_high_rightnew.png")'
       },
    },
    choose: {
        contain: {
          background:'url("/images/background/background_998.jpg")'
         },
        bikinitop: null,
        bikinibottom: null,
        feetStyle: null,
        handbagStyle: null,
        hairStyle: null,
        necklaces: null,
    },
  },
  reducers: {
    handleChooseItem(state, action){
      const { id } = action.payload;
      const existingItem = state.data.find(item => item.id === id)
      if (existingItem) {
        switch(existingItem.type) {
          case "background":
            state.choose.contain = {
              backgroundImage:`url("${existingItem.imgSrc_png}")`,
              backgroundSize: "cover",
              zIndex: "2"
             }
            break;
          case "topclothes":
            state.choose.bikinitop = {
              backgroundImage:`url("${existingItem.imgSrc_png}")`,
              backgroundSize: "cover",
              zIndex: "4",
             }
            break;
          case "botclothes":
            state.choose.bikinibottom = {
              backgroundImage:`url("${existingItem.imgSrc_png}")`,
              backgroundSize: "cover",
              zIndex: "3"
             }
            break;
          case "shoes":
            state.choose.feetStyle = {
              width: "500px",
              height: "1000px",
              backgroundImage:`url("${existingItem.imgSrc_png}")`,
              backgroundSize: "cover",
              position: "absolute",
              bottom: "-40%",
              right: "-3.5%",
              transform: "scale(0.5)",
              zIndex: "5"
             }
          break;
          case "handbags":
            state.choose.handbagStyle = {
              width: "500px",
              height: "1000px",
              backgroundImage:`url("${existingItem.imgSrc_png}")`,
              position: "absolute",
              bottom: "-40.5%",
              right: "-3.5%",
              transform: "scale(0.5)",
              zIndex: "4"
             }

          break;
          case "hairstyle":
            state.choose.hairStyle = {
              width: "1000px",
              height: "1000px",
              backgroundImage:`url("${existingItem.imgSrc_png}")`,
              position: "absolute",
              top: "-75%",
              right: "-57%",
              transform: "scale(0.15)",
              zIndex: "6"
             }
          break;
          case "necklaces":
            state.choose.necklaces = {
              width: "1000px",
              height: "1000px",
              backgroundImage:`url("${existingItem.imgSrc_png}")`,
              position: "absolute",
              top: "-29%",
              right: "-86%",
              transform: "scale(0.52)",
              zIndex: "5",
              backgroundRepeat: "no-repeat",
             }
          break;
          default:
            // code block
        }
      }
    },
    resetChoose(state){
      state.choose.bikinitop = null;
      state.choose.bikinibottom = null;
      state.choose.feetStyle = null;
      state.choose.handbagStyle = null;
      state.choose.hairStyle = null;
      state.choose.necklaces = null;
    }
  },
});

export const { handleChooseItem, resetChoose } = shopSlice.actions;

export const selectShopByType = (state, type) => state.shop.data.filter(item=>item.type === type);
export const selectShopChoose = (state) => (state.shop.choose);
export const selectShopDefaultChoose = (state) => (state.shop.defaultChoose);

export default shopSlice.reducer;
