import React, { useRef, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { selectShopChoose, selectShopDefaultChoose, resetChoose } from '../features/shop/ShopSlice'
export default function Model() {
    const dispatch = useDispatch();
    const [ valueOpacity, setValueOpacity] = useState(1);
    const defaultChoose = useSelector((state)=>selectShopDefaultChoose(state))
    const choose = useSelector((state)=>selectShopChoose(state))
    const { contain, body, bikinitop, bikinibottom, model, feetStyle, handbagStyle, hairStyle, necklaces } = choose
    const bikinitopRef = useRef(null);
    const handleRange =(e)=>{
      const num =  e.target.value
      bikinitopRef.current.style.opacity =  num
      setValueOpacity(e.target.value)
    }
    return (
        <>
        <div className="contain"
        style={contain}>
            <button className="btn-warning"
            onClick={()=>dispatch(resetChoose())}>Quay về bình thường</button>
            <b style={{
              padding:"0 5px"
            }}>Chọn độ dày của áo: </b>
            <input type="range"
             style={{
               width: "100px",
               display: "inline-block"
             }}
               onChange={handleRange}
               min="0.5" max="1" step="0.1"
               value={valueOpacity}
             />


            <div className="body"
            style={defaultChoose.body}>
            </div>
            <div className="model"
            style={defaultChoose.model}>
            </div>
            <div className="bikinitop"
            style={defaultChoose.bikinitop}>
            </div>
            <div className="bikinibottom"
             style={defaultChoose.bikinibottom}>
            </div>
            <div className="feetleft"
            style={defaultChoose.feetleft}>
            </div>
            <div className="feetright"
            style={defaultChoose.feetright}>
          </div>
          <div className="body"
            style={body}>
            </div>
          <div className="model"
            style={model}>
            </div>
          <div className="bikinitop"
            style={bikinitop}
            ref={bikinitopRef}>
            </div>
          <div className="bikinibottom"
                style={bikinibottom}>
            </div>
          <div style={feetStyle}>
            </div>
          <div style={handbagStyle}>
            </div>
          <div style={hairStyle}>
            </div>
          <div style={necklaces}>
            </div>
        </div>
        </>

    )
}