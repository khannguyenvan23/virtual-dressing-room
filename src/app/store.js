import { configureStore } from '@reduxjs/toolkit';
import shopReducer from '../features/shop/ShopSlice';

export default configureStore({
  reducer: {
    shop: shopReducer,
  },
});
