import React from 'react'
import { ShopItems } from '../features/shop/ShopItems'
export default function ShopNav() {
    return (
      <>
        <div className="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">
          <div className="btn-group" role="group">
            <button type="button" id="stars" className="btn btn-primary" href="#tab1" data-toggle="tab">
              <div className="hidden-xs">Áo </div>
            </button>
          </div>
          <div className="btn-group" role="group">
            <button type="button" id="following" className="btn btn-default" href="#tab2" data-toggle="tab">
              <div className="hidden-xs">Quần</div>
            </button>
          </div>
          <div className="btn-group" role="group">
            <button type="button" id="favorites" className="btn btn-default" href="#tab3" data-toggle="tab">
              <div className="hidden-xs">Phụ kiện</div>
            </button>
          </div>
          <div className="btn-group" role="group">
            <button type="button" id="following" className="btn btn-default" href="#tab4" data-toggle="tab">
              <div className="hidden-xs">Tóc </div>
            </button>
          </div>

          <div className="btn-group" role="group">
            <button type="button" id="following" className="btn btn-default" href="#tab5" data-toggle="tab">
              <div className="hidden-xs">Khung nền </div>
            </button>
          </div>
        </div>
        <div className="well">
          <div className="tab-content">
            <div className="tab-pane fade in active" id="tab1">
              <div className="card-group" style={{
                display:'flex',
                flexFlow: "row wrap",
              }}>
                <ShopItems type={"topclothes"}/>
              </div>
            </div>
            <div className="tab-pane fade in" id="tab2">
              <div className="card-group" style={{
                display:'flex',
                flexFlow: "row wrap",
              }}>
                <ShopItems type={"botclothes"}/>
              </div>
            </div>
            <div className="tab-pane fade in" id="tab3">
              <div className="card-group" style={{
                display:'flex',
                flexFlow: "row wrap",
              }}>
                <ShopItems type={"handbags"}/>
                <ShopItems type={"necklaces"}/>
                <ShopItems type={"shoes"}/>
              </div>
            </div>
            <div className="tab-pane fade in" id="tab4">
              <div className="card-group" style={{
                display:'flex',
                flexFlow: "row wrap",
              }}>
                <ShopItems type={"hairstyle"}/>
              </div>
            </div>

            <div className="tab-pane fade in" id="tab5">
            <div className="card-group" style={{
                display:'flex',
                flexFlow: "row wrap",
              }}>
                <ShopItems type={"background"}/>
              </div>
            </div>
          </div>
        </div>
      </>
    )
}